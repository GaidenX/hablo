/* Faceboook api */
function statusChangeCallback(response) {
    console.log(response);
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        GetApiFacebook();
    } else if (response.status === 'not_user_authorized') {
        // The person is logged into Facebook, but not your app.
        console.log('not user_authorized');
    } else {
        // The person is not logged into Facebook, so we're not sure if
        // they are logged into this app or not.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
    }
}

window.fbAsyncInit = function () {
    FB.init({
        appId: '327171111394131',
        xfbml: true,
        version: 'v3.2'
    });

    FB.login();

    // FB.logout(function(response) {
    //     // Person is now logged out
    //  });

    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


function GetApiFacebook() {
    FB.api('/me', 'GET', {fields: 'id,name,picture.width(150).height(150)'}, function (response) {
        console.log(response);
        userChat = response.id;
        userChat = response.name;
        userImage = response.picture.data.url;
    });
}

var userId = 'others';
var userChat = '';
var userImage = 'default';

$(function () {
    var socket = io();

    $('#chat').submit(function (event) {
        event.preventDefault();


        // $('input[name=username]').val()
        var user_image = userImage;
        var user_author = userChat;
        var user_message = $('input[name=message]').val();

        if (user_author.length && user_message.length) {
            var messageObject = {
                user_image: user_image,
                user_author: user_author,
                user_message: user_message
            }
        }
        renderUserMessage(messageObject);
        socket.emit('chatMessage', messageObject);
        // socket.broadcast.emit('receivedMessage', 'me');
    });

    function renderUserMessage(message) {
        $('.messages').append('<div class="user_message"><div><img src="'+ message.user_image +'" width="50" height="50" /><strong>' + message.user_author + ' : </strong>' + message.user_message + '<div></div>')
    }

    socket.on('receivedUser', function (data) {
        userId = data;
    });

    socket.on('receivedMessage', function (message) {
        console.log('another');
        renderUserMessage(message);
    });
});