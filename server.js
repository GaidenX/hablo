// Setup basic express server
var express = require('express');
var app = express();
var path = require('path');
var server = require('http').createServer(app);
var io = require('socket.io')(server);
var porta = process.env.PORT || 3000;

// app.use(express.static(__dirname + '/public'));

// var bodyParser = require('body-parser');
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({
//     extended: true
// }));

app.use(express.static(__dirname + '/public')); //(path.join
app.set('views', path.join(__dirname, 'public'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use('/', function (request, response) {
    response.render('chat.html');
})

let userMessages = [];

var id = 0;

// Add a connect listener
io.on('connection', function(socket) {
    // socket.emit('news', { hello: 'world' });
    // io.emit('some event', { for: 'everyone' });
    //socket.broadcast.emit('hi');
    console.log('Client connected.');

    socket.on('chatMessage', function(data){
        console.log(data);
     
        // io.emit('chat message', data);
        // userMessages.push(data);
        socket.broadcast.emit('receivedMessage', data);
      });

    // Disconnect listener
    socket.on('disconnect', function() {
        console.log('Client disconnected.');
    });
});

// io.on('connection', function(socket) {
//     console.log('socket conectado');
// });

//Paginas - Rotas
// app.get('/chat', function (request, response, next) {
//     response.sendfile('public/chat.html');
// });

server.listen(porta, function () {
    console.log("server on in: " + porta)
});